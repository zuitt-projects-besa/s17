let students = [];

// Add student name to the students array, but check first if the input value is a string or more than 2 letters

function addStudent(studentName){
	if (typeof studentName === "string"){
		
		if (studentName.length <=2) {
			console.log(`Name ${studentName} is too short`)
		} else {
			console.log(`Student ${studentName} was added to the student's list.`)
			students.push(studentName);
		};

	} else {
		console.log(`Name ${studentName} is not valid`)
	}
}


addStudent("Michael Jordan");
addStudent("Steph Curry");
addStudent("Kobe Bryant");
addStudent("Lebron James");
addStudent("zz");
addStudent(123);





// Counts the student enrolled

function countStudents(){
	console.log(`The number of students enrolled is: ${students.length}`)
}
countStudents()




// List the students enrolled
function printStudents(){
	let allStudents = students.join("\n");
	console.log("Name of the Students: \n" + allStudents);
}
printStudents();


// Stretch Goal

function findStudent(nameSearch){
		
		let filteredStudents = students.filter(function(name){
		return name.toLowerCase().includes(nameSearch);
			});

		console.log(`${filteredStudents} is an enrollee`)
		
}

findStudent("kobe");